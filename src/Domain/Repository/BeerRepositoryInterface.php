<?php

declare(strict_types=1);

namespace O2O\Domain\Repository;

use O2O\Domain\Entity\Beer;

interface BeerRepositoryInterface
{
    /**
     * @SuppressWarnings(PHPMD.ShortVariable)
     */
    public function findOneById(int $id): ?Beer;

    /**
     * @return Beer[]
     */
    public function findAllBy(array $criteria): array;
}
