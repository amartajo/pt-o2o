<?php

declare(strict_types=1);

namespace O2O\Application\Beer\Handler;

use O2O\Application\Beer\Dto\ListBeersByFiltersRequest;
use O2O\Application\Common\Service\Validator;
use O2O\Domain\Repository\BeerRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * @SuppressWarnings(PHPMD.ClassNameSuffix)
 */
class ListBeersByFiltersHandler implements MessageHandlerInterface
{
    private Validator $validator;

    private BeerRepositoryInterface $repository;

    public function __construct(Validator $validator, BeerRepositoryInterface $repository)
    {
        $this->validator = $validator;
        $this->repository = $repository;
    }

    public function __invoke(ListBeersByFiltersRequest $request): array
    {
        $this->validator->validate($request);

        if (\array_key_exists('id', $request->getFilters())) {
            $beer = $this->repository->findOneById($request->getFilters()['id']);

            if (null === $beer) {
                return [];
            }

            return [$beer];
        }

        return $this->repository->findAll();
    }
}
