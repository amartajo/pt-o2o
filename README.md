# O2O Technical Test
This is my technical test for Mobile One2One. It is developed with Symfony 5 
using a hexagonal architecture.

I have used PSR1, PSR2, PSR12 and PSR4 standards, plus additional clean code 
recommendations.
In addition, I have added unit and functional tests with PHPUnit.


## Requirements
### Docker
* [Docker](https://docs.docker.com/get-docker/)
* [Docker Compose](https://docs.docker.com/compose/install/) (optional)

### Localhost
* PHP 7.4
* [Composer 2](https://getcomposer.org/download/)
* [Symfony CLI](https://symfony.com/download)


## Installation
### Docker
1. Run `docker build -t pt-o2o .` to build the Docker image for the project.
2. Run `docker run -d -p 8100:80 --name pt-o2o-container pt-o2o` to start a 
   container on port **8100**.

### Docker Compose
1. Run `docker-compose up --build` to build the Docker image for the 
   project and start a container on port **8100**.

### Localhost
1. Run `composer clearcache` and `composer install` to download and install 
   the project dependencies.
2. Run `symfony serve --port 8100` to start a local web server on port 
   **8100** (you can use the port of your choice).


## Main features
* **Swagger:** Use the route `/api/doc.json` to get the Swagger in JSON 
  format (you can use the [Swagger Editor](https://editor.swagger.io/) 
  to have user-friendly interface).
* **Code inspection:** Run `composer lint` to inspect the code with 
  PHP_CodeSniffer, PHP Mess Detector and PHP CS Fixer.
* **Tests:** Run `composer test` to perform the unit and functional tests.
