<?php

declare(strict_types=1);

namespace O2O\Application\Common\Exception;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class BadRequestException extends BadRequestHttpException
{
    private const MESSAGE = 'The data provided is invalid.';

    public function __construct(
        string $message = self::MESSAGE,
        \Throwable $previous = null,
        int $code = 0,
        array $headers = []
    ) {
        parent::__construct($message, $previous, $code, $headers);
    }
}
