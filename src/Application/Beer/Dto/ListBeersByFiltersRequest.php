<?php

declare(strict_types=1);

namespace O2O\Application\Beer\Dto;

use O2O\Application\Common\Dto\RequestInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ListBeersByFiltersRequest implements RequestInterface
{
    /**
     * @Assert\Collection(
     *     fields={
     *         "id"={
     *             @Assert\Type("integer"),
     *             @Assert\Positive()
     *         }
     *     },
     *     allowMissingFields=true
     * )
     */
    private array $filters;

    public function __construct(array $filters)
    {
        $this->filters = $filters;
    }

    public function getFilters(): array
    {
        return $this->filters;
    }
}
