<?php

declare(strict_types=1);

namespace O2O\UI\EventListener;

use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\Messenger\Exception\HandlerFailedException;

class ExceptionListener
{
    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        if ($exception instanceof HandlerFailedException) {
            $this->throwHandledException($exception);
        }
    }

    private function throwHandledException(HandlerFailedException $exception): void
    {
        $handledException = $exception->getPrevious();

        if (null !== $handledException) {
            throw $handledException;
        }
    }
}
