<?php

declare(strict_types=1);

namespace O2O\Application\Beer\Dto;

use O2O\Application\Common\Dto\RequestInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ListBeersBySearchRequest implements RequestInterface
{
    /**
     * @Assert\NotBlank()
     */
    private string $search;

    public function __construct(string $search)
    {
        $this->search = $search;
    }

    public function getSearch(): string
    {
        return $this->search;
    }
}
