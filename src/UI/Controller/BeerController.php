<?php

declare(strict_types=1);

namespace O2O\UI\Controller;

use Nelmio\ApiDocBundle\Annotation\Model;
use O2O\Application\Beer\Dto\ListBeersByFiltersRequest;
use O2O\Application\Beer\Dto\ListBeersBySearchItemResponse;
use O2O\Application\Beer\Dto\ListBeersBySearchRequest;
use O2O\Application\Common\Exception\BadRequestException;
use O2O\Application\Common\Service\BusMessenger;
use O2O\Domain\Entity\Beer;
use OpenApi\Annotations as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BeerController extends AbstractController
{
    /**
     * Returns the beer list.
     *
     * @OA\Tag(name="Beers")
     * @OA\Parameter(
     *     name="filters",
     *     in="query",
     *     description="Criteria for filtering the beer list (e.g. /api/beers/?filters={""id"": 1})
           DO NOT USE IT TOGETHER WITH THE SEARCH!",
     *     @OA\Schema(
     *         type="object",
     *         @OA\Property(
     *             property="id",
     *             type="integer"
     *         )
     *     )
     * )
     * @OA\Parameter(
     *     name="search",
     *     in="query",
     *     description="Search beers by food paired (e.g. /api/beers/?search=chips)
           DO NOT USE IT TOGETHER WITH THE FILTERS!",
     *     @OA\Schema(type="string")
     * )
     * @OA\Response(
     *     response="200",
     *     description="Returns the beer list filtered.
           Using the filters returns all fields while using the search returns a reduced set of fields.",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(
     *             property="data",
     *             type="object",
     *             @OA\Property(
     *                 property="items",
     *                 type="array",
     *                 @OA\Items(oneOf={
     *                     @OA\Schema(ref=@Model(type=Beer::class)),
     *                     @OA\Schema(ref=@Model(type=ListBeersBySearchItemResponse::class))
     *                 })
     *             )
     *         )
     *     )
     * )
     * @OA\Response(response="400", description="Bad Request")
     */
    public function list(Request $request, BusMessenger $busMessenger): JsonResponse
    {
        $filters = json_decode($request->query->get('filters') ?? '{}', true);
        $search = $request->query->get('search') ?? '';

        if (null === $filters || null === $search || (!empty($filters) && !empty($search))) {
            throw new BadRequestException();
        }

        $request = new ListBeersBySearchRequest($search);

        if (!empty($filters)) {
            $request = new ListBeersByFiltersRequest($filters);
        }

        return $this->createResponse($busMessenger->query($request), Response::HTTP_OK);
    }

    private function createResponse(array $itemData, int $httpCode): JsonResponse
    {
        return new JsonResponse(['data' => ['items' => $itemData]], $httpCode);
    }
}
