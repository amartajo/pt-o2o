<?php

declare(strict_types=1);

namespace O2O\Application\Beer\Handler;

use O2O\Application\Beer\Dto\ListBeersBySearchItemResponse;
use O2O\Application\Beer\Dto\ListBeersBySearchRequest;
use O2O\Application\Common\Service\Validator;
use O2O\Domain\Repository\BeerRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * @SuppressWarnings(PHPMD.ClassNameSuffix)
 */
class ListBeersBySearchHandler implements MessageHandlerInterface
{
    private Validator $validator;

    private BeerRepositoryInterface $repository;

    public function __construct(Validator $validator, BeerRepositoryInterface $repository)
    {
        $this->validator = $validator;
        $this->repository = $repository;
    }

    public function __invoke(ListBeersBySearchRequest $request): array
    {
        $this->validator->validate($request);

        $beers = $this->repository->findAllBy(['food' => $request->getSearch()]);
        $response = [];

        foreach ($beers as $beer) {
            $response[] = new ListBeersBySearchItemResponse(
                $beer->getId(),
                $beer->getName(),
                $beer->getDescription()
            );
        }

        return $response;
    }
}
