<?php

declare(strict_types=1);

namespace O2O\Application\Common\Service;

use O2O\Application\Common\Dto\RequestInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;

class BusMessenger
{
    private MessageBusInterface $bus;

    public function __construct(MessageBusInterface $bus)
    {
        $this->bus = $bus;
    }

    /**
     * @return mixed
     */
    public function query(RequestInterface $request)
    {
        $envelope = $this->bus->dispatch($request);
        /** @var HandledStamp $response */
        $response = $envelope->last(HandledStamp::class);

        return $response->getResult();
    }
}
