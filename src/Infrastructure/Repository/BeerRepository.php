<?php

declare(strict_types=1);

namespace O2O\Infrastructure\Repository;

use O2O\Domain\Entity\Beer;
use O2O\Domain\Repository\BeerRepositoryInterface;
use O2O\Infrastructure\Service\BeerClientInterface;

class BeerRepository implements BeerRepositoryInterface
{
    private BeerClientInterface $beerClient;

    public function __construct(BeerClientInterface $beerClient)
    {
        $this->beerClient = $beerClient;
    }

    /**
     * @SuppressWarnings(PHPMD.ShortVariable)
     */
    public function findOneById(int $id): ?Beer
    {
        $data = $this->beerClient->findBeerById($id);

        if (null === $data) {
            return null;
        }

        return $this->createBeerFromData($data);
    }

    public function findAllBy(array $criteria): array
    {
        $beers = [];

        $beersData = $this->beerClient->findBeersBy($criteria);

        foreach ($beersData as $data) {
            $beers[] = $this->createBeerFromData($data);
        }

        return $beers;
    }

    private function createBeerFromData(array $data): Beer
    {
        return new Beer(
            $data['id'],
            $data['name'],
            $data['description'],
            $data['image_url'] ?? '',
            $data['tagline'],
            $data['first_brewed']
        );
    }
}
