<?php

declare(strict_types=1);

namespace O2O\Infrastructure\Service;

interface BeerClientInterface
{
    /**
     * @SuppressWarnings(PHPMD.ShortVariable)
     */
    public function findBeerById(int $id): ?array;

    public function findBeersBy(array $criteria): array;
}
