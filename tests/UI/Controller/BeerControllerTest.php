<?php

declare(strict_types=1);

namespace O2O\Tests\UI\Controller;

use O2O\Application\Common\Exception\BadRequestException;
use O2O\Infrastructure\Service\BeerClientInterface;
use O2O\Infrastructure\Service\PunkApiClient;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\RouterInterface;

/**
 * @SuppressWarnings(PHPMD.PrivateFieldDeclaration)
 */
class BeerControllerTest extends WebTestCase
{
    private KernelBrowser $httpClient;

    private RouteCollection $routeCollection;

    protected function setUp(): void
    {
        $this->httpClient = self::createClient();

        /** @var RouterInterface $router */
        $router = $this->httpClient->getContainer()->get('router');
        $this->routeCollection = $router->getRouteCollection();
    }

    /**
     * @dataProvider provideTestListBeersWithValidRequestData
     */
    public function testListBeersWithValidRequest(array $data): void
    {
        $this->setBeerClientMock($data['mock']);
        $this->httpClient->request(
            Request::METHOD_GET,
            $this->getPath($this->httpClient->getContainer()->getParameter('o2o.beer_list_route')),
            $data['request']
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        $expectedResponse = $this->parseJsonResource('Response/' . $data['response']);

        $this->assertSame(
            json_decode($expectedResponse, true),
            json_decode($this->httpClient->getResponse()->getContent(), true)
        );
    }

    public function provideTestListBeersWithValidRequestData(): array
    {
        return [
            'id_filter' => [
                [
                    'request' => [
                        'filters' => json_encode(['id' => 1]),
                    ],
                    'response' => 'beerListWithOneResult.json',
                    'mock' => 'beerMock.json',
                ],
            ],
            'search' => [
                [
                    'request' => [
                        'search' => 'peanut',
                    ],
                    'response' => 'beerListWithOneSearchedResult.json',
                    'mock' => 'beerMock.json',
                ],
            ],
        ];
    }

    /**
     * @dataProvider provideTestListBeersWithInvalidRequestData
     */
    public function testListBeersWithInvalidRequest(array $data): void
    {
        $this->expectException($data['exception']);

        $this->httpClient->request(
            Request::METHOD_GET,
            $this->getPath($this->httpClient->getContainer()->getParameter('o2o.beer_list_route')),
            $data['request']
        );
    }

    public function provideTestListBeersWithInvalidRequestData(): array
    {
        return [
            'invalid_filter' => [
                [
                    'exception' => BadRequestException::class,
                    'request' => [
                        'filters' => json_encode(['id' => -1]),
                    ],
                ],
            ],
            'invalid_search' => [
                [
                    'exception' => BadRequestException::class,
                    'request' => [
                        'search' => '',
                    ],
                ],
            ],
        ];
    }

    private function getPath(string $route): string
    {
        return $this->routeCollection->get($route)->getPath();
    }

    private function setBeerClientMock(string $responseFile): void
    {
        $this->httpClient->getContainer()->set(BeerClientInterface::class, new PunkApiClient(
            new MockHttpClient([new MockResponse($this->parseJsonResource('Mock/' . $responseFile))]),
            $this->httpClient->getContainer()->getParameter('o2o.beer_provider_config')
        ));
    }

    private function parseJsonResource(string $resourceFile): string
    {
        return file_get_contents(sprintf(
            '%s/%s',
            $this->httpClient->getContainer()->getParameter('o2o.functional_tests_resources'),
            $resourceFile
        ));
    }
}
