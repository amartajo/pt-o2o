<?php

declare(strict_types=1);

namespace O2O\Infrastructure\Service;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

/**
 * @SuppressWarnings(PHPMD.PrivateFieldDeclaration)
 */
class PunkApiClient implements BeerClientInterface
{
    private const PROVIDER_CONFIG = [
        'FIND_BEER_URL' => 'find_beer_url',
    ];

    private HttpClientInterface $httpClient;

    /**
     * @var string[]
     */
    private array $beerProviderConfig;

    /**
     * @param string[] $apiUrls
     */
    public function __construct(HttpClientInterface $httpClient, array $beerProviderConfig)
    {
        $this->httpClient = $httpClient;
        $this->beerProviderConfig = $beerProviderConfig;
    }

    /**
     * @SuppressWarnings(PHPMD.ShortVariable)
     */
    public function findBeerById(int $id): ?array
    {
        $response = $this->request(
            Request::METHOD_GET,
            sprintf('%s/%s', $this->beerProviderConfig[self::PROVIDER_CONFIG['FIND_BEER_URL']], $id)
        );

        if (Response::HTTP_NOT_FOUND === $response->getStatusCode()) {
            return null;
        }

        return $response->toArray()[0];
    }

    public function findBeersBy(array $criteria): array
    {
        $response = $this->request(
            Request::METHOD_GET,
            $this->getUrlWithParameters($this->beerProviderConfig[self::PROVIDER_CONFIG['FIND_BEER_URL']], $criteria),
        );

        return $response->toArray();
    }

    private function request(string $method, string $url): ResponseInterface
    {
        return $this->httpClient->request($method, $url);
    }

    private function getUrlWithParameters(string $url, array $parameters): string
    {
        $url .= '?';

        foreach ($parameters as $parameter => $value) {
            $url .= sprintf('%s=%s&', $parameter, $value);
        }

        return trim($url, '&');
    }
}
