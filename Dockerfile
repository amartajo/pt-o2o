FROM php:7.4-apache

RUN apt update && \
    apt install git -y

COPY ./docker-config/apache/sites-available/000-default.conf /etc/apache2/sites-available/000-default.conf

RUN mkdir -p /app
COPY . /app
WORKDIR /app

COPY --from=composer /usr/bin/composer /usr/bin/composer
RUN composer install --optimize-autoloader && \
    composer require symfony/apache-pack && \
    composer lint && \
    composer test

EXPOSE 80
