<?php

declare(strict_types=1);

namespace O2O\Domain\Entity;

use JsonSerializable;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 * @SuppressWarnings(PHPMD.DataStructureMethods)
 */
class Beer implements JsonSerializable
{
    private int $id;

    private string $name;

    private string $description;

    private string $image;

    private string $tagline;

    private string $firstBrewed;

    public function __construct(
        int $id,
        string $name,
        string $description,
        string $image,
        string $tagline,
        string $firstBrewed
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->image = $image;
        $this->tagline = $tagline;
        $this->firstBrewed = $firstBrewed;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function getTagline(): string
    {
        return $this->tagline;
    }

    public function getFirstBrewed(): string
    {
        return $this->firstBrewed;
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}
