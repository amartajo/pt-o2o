<?php

declare(strict_types=1);

namespace O2O\Application\Common\Service;

use O2O\Application\Common\Exception\BadRequestException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class Validator
{
    private ValidatorInterface $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function validate(object $object): void
    {
        $errors = $this->validator->validate($object);

        if ($errors->count() > 0) {
            throw new BadRequestException();
        }
    }
}
