<?php

declare(strict_types=1);

namespace O2O\Tests\Application\Common\Service;

use O2O\Application\Beer\Dto\ListBeersByFiltersRequest;
use O2O\Application\Beer\Dto\ListBeersBySearchRequest;
use O2O\Application\Common\Dto\RequestInterface;
use O2O\Application\Common\Exception\BadRequestException;
use O2O\Application\Common\Service\Validator;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ValidatorTest extends WebTestCase
{
    protected function setUp(): void
    {
        self::bootKernel();
    }

    /**
     * @dataProvider provideTestRequestsWithValidInputData
     * @doesNotPerformAssertions
     */
    public function testRequestsWithValidInput(RequestInterface $data): void
    {
        $validator = new Validator(self::$container->get(ValidatorInterface::class));
        $validator->validate($data);
    }

    public function provideTestRequestsWithValidInputData(): array
    {
        return [
            'beer_list_with_valid_id' => [new ListBeersByFiltersRequest(['id' => 1])],
            'beer_list_with_valid_search' => [new ListBeersBySearchRequest('peanut')],
        ];
    }

    /**
     * @dataProvider provideTestRequestsWithInvalidInputData
     */
    public function testRequestsWithInvalidInput(RequestInterface $data): void
    {
        $this->expectException(BadRequestException::class);

        $validator = new Validator(self::$container->get(ValidatorInterface::class));
        $validator->validate($data);
    }

    public function provideTestRequestsWithInvalidInputData(): array
    {
        return [
            'beer_list_with_invalid_id_float' => [new ListBeersByFiltersRequest(['id' => 0.1])],
            'beer_list_with_invalid_id_negative' => [new ListBeersByFiltersRequest(['id' => -1])],
            'beer_list_with_invalid_id_string' => [new ListBeersByFiltersRequest(['id' => 'a'])],
            'beer_list_with_invalid_search_empty' => [new ListBeersBySearchRequest('')],
        ];
    }
}
